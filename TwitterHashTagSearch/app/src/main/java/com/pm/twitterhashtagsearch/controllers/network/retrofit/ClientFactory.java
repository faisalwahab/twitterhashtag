package com.pm.twitterhashtagsearch.controllers.network.retrofit;

/**
 * Created by faisalwahab on 3/21/16.
 */
public class ClientFactory {
    public enum ClientType {
        SIMPLE, CACHE
    }

    private RestClient restClientWithoutCache = null;
    private RestClientCache restClientWithCache = null;

    private static ClientFactory INSTANCE = null;

    public static ClientFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientFactory();
        }
        return INSTANCE;
    }


    public IResetClient getClient(ClientType clientType) {
        if (clientType != null) {
            if (clientType == ClientType.SIMPLE) {
                if (restClientWithoutCache == null) {
                    restClientWithoutCache = new RestClient();
                }
                return restClientWithoutCache;
            } else if (clientType == ClientType.CACHE) {
                if (restClientWithCache == null) {
                    restClientWithCache = new RestClientCache();
                }
                return restClientWithCache;
            }
        }
        return null;
    }
}
