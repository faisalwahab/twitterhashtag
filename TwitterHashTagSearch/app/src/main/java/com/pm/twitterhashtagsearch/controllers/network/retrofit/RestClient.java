package com.pm.twitterhashtagsearch.controllers.network.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by faisalwahab on 1/31/16.
 */
public class RestClient implements IResetClient {
    private Retrofit retrofit;

    public RestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        // Add Logging's in Api Calling
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);  // <-- this is the important line!


        retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
    }

    @Override
    public <T> T getRetrofitService(final Class<T> clazz) {
        return retrofit.create(clazz); //  T service = retrofit.create(clazz);
    }


    /**
     * Get the base url of the api endpoint.
     */
    public String getBaseUrl() {
        return "https://api.twitter.com/1.1/";
    }

}