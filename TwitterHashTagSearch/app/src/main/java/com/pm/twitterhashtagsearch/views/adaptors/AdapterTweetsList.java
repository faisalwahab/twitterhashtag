package com.pm.twitterhashtagsearch.views.adaptors;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.twitterhashtagsearch.R;
import com.pm.twitterhashtagsearch.controllers.interfaces.ICallBack;
import com.pm.twitterhashtagsearch.controllers.utilities.ApplicationUtils;
import com.pm.twitterhashtagsearch.models.ModelSearchTweetHashTags;
import com.pm.twitterhashtagsearch.models.ModelSearchTweetsStatus;
import com.pm.twitterhashtagsearch.models.ModelSearchTweetsStatusUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by faisalwahab on 4/5/16.
 */
public class AdapterTweetsList extends RecyclerView.Adapter<AdapterTweetsList.MyViewHolder> {

    private ICallBack iCallBack;
    private List<ModelSearchTweetsStatus> tweets = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_tweeted_by_img;
        public TextView tv_tweeted_by_name, tv_tweeted_by_id, tv_tweet_text, tv_tweet_hashtags, tv_retweet_count;

        public MyViewHolder(View view) {
            super(view);
            tv_tweeted_by_name = (TextView) view.findViewById(R.id.tv_tweeted_by_name);
            tv_tweeted_by_id = (TextView) view.findViewById(R.id.tv_tweeted_by_id);
            tv_tweet_text = (TextView) view.findViewById(R.id.tv_tweet_text);
            tv_tweet_hashtags = (TextView) view.findViewById(R.id.tv_tweet_hashtags);
            tv_retweet_count = (TextView) view.findViewById(R.id.tv_retweet_count);
            iv_tweeted_by_img = (ImageView) view.findViewById(R.id.iv_tweeted_by_img);
        }
    }


    public AdapterTweetsList(List<ModelSearchTweetsStatus> tweets, ICallBack iCallBack) {
        this.tweets = tweets;
        this.iCallBack = iCallBack;
    }

    public AdapterTweetsList(List<ModelSearchTweetsStatus> tweets) {
        this.tweets = tweets;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptor_item_tweets, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (tweets != null) {
            ModelSearchTweetsStatus tweet = tweets.get(position);
            if (tweet != null) {

                ModelSearchTweetsStatusUser tweetBy = tweet.getUser();
                if (tweetBy != null) {
                    holder.tv_tweeted_by_name.setText(tweetBy.getName());
                    holder.tv_tweeted_by_id.setText("@" + tweetBy.getScreen_name());
                }
                holder.tv_tweet_text.setText(tweet.getText());

                if (tweet.getEntities() != null) {
                    List<ModelSearchTweetHashTags> listHashTag = tweet.getEntities().getHashtags();

                    SpannableStringBuilder spannableHashTagStringBuilder = new SpannableStringBuilder();
                    for (int i = 0; i < listHashTag.size(); i++) {
                        final SpannableString spannableHashTag = new SpannableString("#" + listHashTag.get(i).getText() + " ");
                        spannableHashTag.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View view) {
                                if (iCallBack != null) {
                                    iCallBack.onCallBack(AdapterTweetsList.this, spannableHashTag.toString(), "click_hash_tag", 0);
                                }
                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                ds.setUnderlineText(false);
                            }
                        }, 0, spannableHashTag.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        spannableHashTagStringBuilder.append(spannableHashTag);
                    }
                    holder.tv_tweet_hashtags.setMovementMethod(LinkMovementMethod.getInstance());
                    holder.tv_tweet_hashtags.setText(spannableHashTagStringBuilder);
                }

                String retweetCount = " RETWEET";
                if (tweet.getRetweet_count() > 1) {
                    retweetCount = " RETWEETS";
                }
                holder.tv_retweet_count.setText(tweet.getRetweet_count() + retweetCount);

                ApplicationUtils.loadImages(holder.iv_tweeted_by_img, tweet.getUser().getProfile_image_url());

            }
        }
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }
}