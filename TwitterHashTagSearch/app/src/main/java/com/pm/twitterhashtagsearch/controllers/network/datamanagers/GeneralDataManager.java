package com.pm.twitterhashtagsearch.controllers.network.datamanagers;

import android.support.v4.app.FragmentActivity;

import com.pm.twitterhashtagsearch.controllers.network.retrofit.ClientFactory;
import com.pm.twitterhashtagsearch.controllers.network.retrofit.IRetrofitCallBack;
import com.pm.twitterhashtagsearch.controllers.network.utils.ApiConstants;
import com.pm.twitterhashtagsearch.models.ModelSearchTweets;

import retrofit2.Call;

/**
 * Created by faisalwahab on 1/31/16.
 */
public class GeneralDataManager extends BaseDataManager {
    public String FEATCH_TWEETS_BY_HASHTAG = "FEATCH_TWEETS_BY_HASHTAG";

    private static GeneralDataManager INSTANCE = null;

    public static GeneralDataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GeneralDataManager();
        }
        return INSTANCE;
    }


    public void fetchTweetsListByHashTag(String hashTag, String since_id, String count, final IRetrofitCallBack iRetrofitCallBack, boolean isProgressDialogShow, final FragmentActivity activity) {
        if (!hashTag.contains("#")) {
            hashTag = "#" + hashTag;
        }
        hashTag = hashTag.replace("#", "%23");

        Call<ModelSearchTweets> call = getGeneralsService(ClientFactory.ClientType.SIMPLE).fetchTweets(hashTag, since_id, count);
        retrofit2Api(call, new IRetrofitCallBack() {
            @Override
            public void onCallBack(Object ref, Object data, Object action, int status) {
                if (data != null) {

                    if (iRetrofitCallBack != null) {
                        iRetrofitCallBack.onCallBack(GeneralDataManager.this, data, FEATCH_TWEETS_BY_HASHTAG, ApiConstants.SUCCESS);
                    }

                }
            }
        }, isProgressDialogShow, activity);
    }

}
