package com.pm.twitterhashtagsearch.models;

import java.io.Serializable;

public class ModelImageViewSize implements Serializable {
	private static final long serialVersionUID = 1L;

	private int width = 0;
	private int height = 0;

	public ModelImageViewSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "ModelImageViewSize [width=" + width + ", height=" + height
				+ "]";
	}
}
