package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweetsStatusEntitles implements Parcelable {
    private List<ModelSearchTweetHashTags> hashtags;

    public ModelSearchTweetsStatusEntitles() {
    }

    public List<ModelSearchTweetHashTags> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<ModelSearchTweetHashTags> hashtags) {
        this.hashtags = hashtags;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.hashtags);
    }

    protected ModelSearchTweetsStatusEntitles(Parcel in) {
        this.hashtags = in.createTypedArrayList(ModelSearchTweetHashTags.CREATOR);
    }

    public static final Parcelable.Creator<ModelSearchTweetsStatusEntitles> CREATOR = new Parcelable.Creator<ModelSearchTweetsStatusEntitles>() {
        @Override
        public ModelSearchTweetsStatusEntitles createFromParcel(Parcel source) {
            return new ModelSearchTweetsStatusEntitles(source);
        }

        @Override
        public ModelSearchTweetsStatusEntitles[] newArray(int size) {
            return new ModelSearchTweetsStatusEntitles[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweetsStatusEntitles{" +
                "hashtags=" + hashtags +
                '}';
    }
}
