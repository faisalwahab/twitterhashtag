package com.pm.twitterhashtagsearch.views.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.pm.twitterhashtagsearch.R;
import com.pm.twitterhashtagsearch.controllers.interfaces.ICallBack;
import com.pm.twitterhashtagsearch.controllers.network.datamanagers.GeneralDataManager;
import com.pm.twitterhashtagsearch.controllers.network.retrofit.IRetrofitCallBack;
import com.pm.twitterhashtagsearch.controllers.utilities.ApplicationUtils;
import com.pm.twitterhashtagsearch.controllers.utilities.EndlessRecyclerViewScrollListener;
import com.pm.twitterhashtagsearch.models.ModelSearchTweets;
import com.pm.twitterhashtagsearch.models.ModelSearchTweetsStatus;
import com.pm.twitterhashtagsearch.views.adaptors.AdapterTweetsList;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityMain extends ActivityBase implements ICallBack {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private LinearLayout layout_loadmore;

    private List<ModelSearchTweetsStatus> tweets = new ArrayList<>();
    private AdapterTweetsList mAdapter;

    private SearchView searchView;
    private String queryText = "";

    private Timer timer;

    private String SINCE_ID = "";
    private String INCREMENT_INDEX = "25";

    private ModelSearchTweets tweetsSearchResult;

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_screen_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                String query = s;
                clearTweetList();

                queryText = query;
                if (ApplicationUtils.isQueryValid(queryText)) {
                    getTweetsByHashTag(queryText, SINCE_ID, INCREMENT_INDEX, false);
                }
                return false;
            }
        });

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActionBar(this, savedInstanceState);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.rv_tweets);
        layout_loadmore = (LinearLayout) findViewById(R.id.layout_loadmore);

        mAdapter = new AdapterTweetsList(tweets, this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ApplicationUtils.getAppContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                customLoadMoreDataFromApi();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshTweetsList();
            }
        });
        pullToRefreshEnable(true);

        setSelectedMenuOption(3);
    }

    // Append more data into the adapter
    // This method probably sends out a network request and appends new data items to your adapter.
    public void customLoadMoreDataFromApi() {
        if (tweetsSearchResult != null && tweetsSearchResult.getSearch_metadata() != null) {
            SINCE_ID = String.valueOf(tweetsSearchResult.getSearch_metadata().getMax_id_str());
        }
        if (ApplicationUtils.isQueryValid(queryText)) {
            getTweetsByHashTag(queryText, SINCE_ID, INCREMENT_INDEX, true);
        }
    }

    public void pullToRefreshEnable(boolean enable) {
        mSwipeRefreshLayout.setEnabled(enable);
    }

    public void setRefreshTimerIntervalForTweets(int refreshTimeIntervalInSec) {
        if (timer != null) {
            timer.cancel();
        }

        if (refreshTimeIntervalInSec != 0) {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshTweetsList();
                        }
                    });

                }
            }, refreshTimeIntervalInSec * 1000, refreshTimeIntervalInSec * 1000);
        }
    }

    private void refreshTweetsList() {
        clearTweetList();
        if (ApplicationUtils.isQueryValid(queryText)) {
            getTweetsByHashTag(queryText, SINCE_ID, INCREMENT_INDEX, false);
        }
    }

    private void clearTweetList() {
        SINCE_ID = "";

        if (tweets != null && mAdapter != null) {
            tweets.clear();
            mAdapter.notifyDataSetChanged();
        }
    }


    private void getTweetsByHashTag(String hashTag, String since_id, String count, boolean isLoadMoreShow) {
        if (isLoadMoreShow && layout_loadmore != null) {
            layout_loadmore.setVisibility(View.VISIBLE);
        }

        GeneralDataManager.getInstance().fetchTweetsListByHashTag(hashTag, since_id, count, new IRetrofitCallBack() {
            @Override
            public void onCallBack(Object ref, Object data, Object action, int status) {
                if (queryText.length() > 0) {
                    tweetsSearchResult = ((ModelSearchTweets) data);

                    if (tweetsSearchResult != null) {
                        if (tweetsSearchResult.getStatuses() != null) {
                            tweets.addAll(tweetsSearchResult.getStatuses());
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }

                if (layout_loadmore != null) {
                    layout_loadmore.setVisibility(View.GONE);
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, false, this);
    }

    @Override
    public void onCallBack(Object ref, Object data, Object action, int status) {
        if (ref instanceof AdapterTweetsList) {
            String hashTag = (String) data;

            if (searchView != null) {
                searchView.setQuery(hashTag.replace("#", ""), false);
            }
        }
    }
}
