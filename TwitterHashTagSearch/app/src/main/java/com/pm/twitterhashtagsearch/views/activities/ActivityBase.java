package com.pm.twitterhashtagsearch.views.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.pm.twitterhashtagsearch.R;


/**
 * Created by faisalwahab on 1/31/16.
 */
public class ActivityBase extends AppCompatActivity {
    public FragmentActivity mActivity = null;

    private Toolbar mToolBar = null;
    private Drawer drawerMainMenu = null;

    public ActivityBase() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
    }

    public void setupActionBar(Activity activity, Bundle savedInstanceState) {
        mToolBar = (Toolbar) findViewById(R.id.tool_bar);

        setDrawer(activity, savedInstanceState);

        mToolBar.inflateMenu(R.menu.main_screen_menu);
        setSupportActionBar(mToolBar);

        mToolBar.setNavigationOnClickListener(navigationListenerMainMenu);
    }

    private void setDrawer(Activity activity, Bundle savedInstanceState) {
        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem refresh_interval = new PrimaryDrawerItem().withIdentifier(6).withName("REFRESH INTERVAL").withSelectable(false);
        PrimaryDrawerItem no_refresh = new PrimaryDrawerItem().withIdentifier(1).withName("No Refresh").withIcon(getResources().getDrawable(R.drawable.ic_sync_disabled));
        PrimaryDrawerItem refresh_after_every_2_sec = new PrimaryDrawerItem().withIdentifier(2).withName("2 Second").withIcon(getResources().getDrawable(R.drawable.ic_sync));
        PrimaryDrawerItem refresh_after_every_5_sec = new PrimaryDrawerItem().withIdentifier(3).withName("5 Second").withIcon(getResources().getDrawable(R.drawable.ic_sync));
        PrimaryDrawerItem refresh_after_every_30_sec = new PrimaryDrawerItem().withIdentifier(4).withName("30 Second").withIcon(getResources().getDrawable(R.drawable.ic_sync));
        PrimaryDrawerItem refresh_after_every_1_min = new PrimaryDrawerItem().withIdentifier(5).withName("1 Minute").withIcon(getResources().getDrawable(R.drawable.ic_sync));

        // Handle Toolbar
        drawerMainMenu = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(mToolBar)
                .withHeader(R.layout.layout_side_menu_header)
                .withSavedInstance(savedInstanceState)
                .withDisplayBelowStatusBar(true)
                .withTranslucentStatusBar(false)
                .withInnerShadow(true)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .addDrawerItems(
                        refresh_interval,
                        new DividerDrawerItem(),
                        no_refresh,
                        refresh_after_every_2_sec,
                        refresh_after_every_5_sec,
                        refresh_after_every_30_sec,
                        refresh_after_every_1_min
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        boolean isPullToRefreshEnable = false;
                        int refreshTimeIntervalInSec = 0;

                        if (position == 3) {
                            isPullToRefreshEnable = true;
                            refreshTimeIntervalInSec = 0;
                        } else if (position == 4) {
                            refreshTimeIntervalInSec = 2;
                        } else if (position == 5) {
                            refreshTimeIntervalInSec = 5;
                        } else if (position == 6) {
                            refreshTimeIntervalInSec = 30;
                        } else if (position == 7) {
                            refreshTimeIntervalInSec = 60;
                        }

                        ((ActivityMain) mActivity).pullToRefreshEnable(isPullToRefreshEnable);
                        ((ActivityMain) mActivity).setRefreshTimerIntervalForTweets(refreshTimeIntervalInSec);

                        return false;
                    }
                }).build();
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (drawerMainMenu != null && drawerMainMenu.isDrawerOpen()) {
            drawerMainMenu.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public void setSelectedMenuOption(int selectedMenuOption) {
        if (drawerMainMenu != null) {
            drawerMainMenu.setSelection(selectedMenuOption, true);
        }
    }

    public View.OnClickListener navigationListenerMainMenu = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (drawerMainMenu != null && !drawerMainMenu.isDrawerOpen()) {
                drawerMainMenu.openDrawer();
            }
        }
    };
}
