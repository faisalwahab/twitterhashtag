package com.pm.twitterhashtagsearch.controllers.network.retrofit;

import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pm.twitterhashtagsearch.controllers.network.utils.ApiConstants;
import com.pm.twitterhashtagsearch.views.dialogs.DialogsManager;

public class ErrorUtils {

    public static void parseError(int code, String message, final IRetrofitCallBack iCallBack, final FragmentActivity activity) {

        if (code == ApiConstants.FAILURE) {
            if (message.equalsIgnoreCase(ApiConstants.ERROR_NETWORK_NOT_AVAILABLE)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        DialogsManager.getInstance().internetNotAvailable(activity, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                switch (which) {
                                    case POSITIVE:
                                        if (iCallBack != null) {
                                            iCallBack.onCallBack(null, null, null, ApiConstants.RESET);
                                        }
                                        break;
                                }
                            }
                        });

                    }
                });
            }
        }

    }
}
