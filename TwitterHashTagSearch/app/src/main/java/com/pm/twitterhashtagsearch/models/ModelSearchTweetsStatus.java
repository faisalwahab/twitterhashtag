package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweetsStatus implements Parcelable {
    private String created_at;
    private long id;
    private String id_str;
    private String text;
    private ModelSearchTweetsStatusEntitles entities;
    private String source;
    private ModelSearchTweetsStatusUser user;
    private int retweet_count;
    private int favorite_count;
    private String lang;

    public ModelSearchTweetsStatus() {
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_str() {
        return id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ModelSearchTweetsStatusEntitles getEntities() {
        return entities;
    }

    public void setEntities(ModelSearchTweetsStatusEntitles entities) {
        this.entities = entities;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ModelSearchTweetsStatusUser getUser() {
        return user;
    }

    public void setUser(ModelSearchTweetsStatusUser user) {
        this.user = user;
    }

    public int getRetweet_count() {
        return retweet_count;
    }

    public void setRetweet_count(int retweet_count) {
        this.retweet_count = retweet_count;
    }

    public int getFavorite_count() {
        return favorite_count;
    }

    public void setFavorite_count(int favorite_count) {
        this.favorite_count = favorite_count;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.created_at);
        dest.writeLong(this.id);
        dest.writeString(this.id_str);
        dest.writeString(this.text);
        dest.writeParcelable(this.entities, flags);
        dest.writeString(this.source);
        dest.writeParcelable(this.user, flags);
        dest.writeInt(this.retweet_count);
        dest.writeInt(this.favorite_count);
        dest.writeString(this.lang);
    }

    protected ModelSearchTweetsStatus(Parcel in) {
        this.created_at = in.readString();
        this.id = in.readLong();
        this.id_str = in.readString();
        this.text = in.readString();
        this.entities = in.readParcelable(ModelSearchTweetsStatusEntitles.class.getClassLoader());
        this.source = in.readString();
        this.user = in.readParcelable(ModelSearchTweetsStatusUser.class.getClassLoader());
        this.retweet_count = in.readInt();
        this.favorite_count = in.readInt();
        this.lang = in.readString();
    }

    public static final Creator<ModelSearchTweetsStatus> CREATOR = new Creator<ModelSearchTweetsStatus>() {
        @Override
        public ModelSearchTweetsStatus createFromParcel(Parcel source) {
            return new ModelSearchTweetsStatus(source);
        }

        @Override
        public ModelSearchTweetsStatus[] newArray(int size) {
            return new ModelSearchTweetsStatus[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweetsStatus{" +
                "created_at='" + created_at + '\'' +
                ", id=" + id +
                ", id_str='" + id_str + '\'' +
                ", text='" + text + '\'' +
                ", entities=" + entities +
                ", source='" + source + '\'' +
                ", user=" + user +
                ", retweet_count=" + retweet_count +
                ", favorite_count=" + favorite_count +
                ", lang='" + lang + '\'' +
                '}';
    }
}
