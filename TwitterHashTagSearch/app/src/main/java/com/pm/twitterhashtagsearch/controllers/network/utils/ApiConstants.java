package com.pm.twitterhashtagsearch.controllers.network.utils;

/**
 * Created by faisalwahab on 2/7/16.
 */
public class ApiConstants {
    public static int FAILURE = 0;
    public static int SUCCESS = 1;
    public static int RESET = 2;

   public static String ERROR_NETWORK_NOT_AVAILABLE = "error_network_not_available";
}
