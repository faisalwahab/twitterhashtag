package com.pm.twitterhashtagsearch.controllers.interfaces;

/**
 * Created by faisalwahab on 1/31/16.
 */
public interface ICallBack {
    void onCallBack(Object ref, Object data, Object action, int status);
}