package com.pm.twitterhashtagsearch.controllers.network.retrofit;

/**
 * Created by faisalwahab on 3/21/16.
 */
public interface IResetClient {
    <T> T getRetrofitService(Class<T> serviceClass);
}
