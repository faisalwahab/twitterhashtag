package com.pm.twitterhashtagsearch.controllers.network.retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pm.twitterhashtagsearch.controllers.utilities.ApplicationUtils;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by faisalwahab on 1/31/16.
 */
public class RestClientCache implements IResetClient {
    private Retrofit retrofit;

    public RestClientCache() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        File httpCacheDirectory = new File(ApplicationUtils.getAppContext().getCacheDir(), "BloodCartOKHttpCache");
        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024); // 10 MB cache limit
        httpClient.cache(cache);

        httpClient.addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR);

        retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
    }

    @Override
    public <T> T getRetrofitService(final Class<T> clazz) {
        return retrofit.create(clazz); //  T service = retrofit.create(clazz);
    }


    /**
     * Get the base url of the api endpoint.
     */
    public String getBaseUrl() {
        return "https://api.twitter.com/1.1/";
    }

    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .removeHeader("Pragma")
                    .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", 1 * 60 * 60 * 24, 0))  // cache for 1 day
                    .build();
        }
    };


}