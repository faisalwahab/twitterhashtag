package com.pm.twitterhashtagsearch.views.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pm.twitterhashtagsearch.R;

/**
 * Created by faisalwahab on 4/6/16.
 */
public class DialogsManager {

    private static DialogsManager INSTANCE = null;

    public static DialogsManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new DialogsManager();
        return INSTANCE;
    }

    private DialogsManager() {
    }


    public MaterialDialog indeterminateProgressDialog(Context context) {
        return new MaterialDialog.Builder(context)
                .content("Loading please wait...")
                .progress(true, 0)
                .show();
    }

    public void internetNotAvailable(FragmentActivity activity, final MaterialDialog.SingleButtonCallback iCallBack) {
        if (activity != null) {
            new MaterialDialog.Builder(activity)
                    .title(R.string.txt_dialog_internet_error_title)
                    .content(R.string.txt_dialog_internet_error_message)
                    .positiveText(R.string.txt_retry)
                    .negativeText(R.string.txt_cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (iCallBack != null)
                                iCallBack.onClick(dialog, which);
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (iCallBack != null)
                                iCallBack.onClick(dialog, which);
                        }
                    })
                    .canceledOnTouchOutside(false)
                    .show();
        }
    }

}
