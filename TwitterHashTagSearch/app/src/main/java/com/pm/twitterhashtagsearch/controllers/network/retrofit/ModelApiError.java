package com.pm.twitterhashtagsearch.controllers.network.retrofit;

/**
 * Created by faisalwahab on 2/7/16.
 */
public class ModelApiError {
    private String status;
    private String message;

    public ModelApiError() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ModelApiError{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
