package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweets implements Parcelable {

    private List<ModelSearchTweetsStatus> statuses;
    private ModelSearchTweetsMetaData search_metadata;

    public ModelSearchTweets() {
    }

    public List<ModelSearchTweetsStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ModelSearchTweetsStatus> statuses) {
        this.statuses = statuses;
    }

    public ModelSearchTweetsMetaData getSearch_metadata() {
        return search_metadata;
    }

    public void setSearch_metadata(ModelSearchTweetsMetaData search_metadata) {
        this.search_metadata = search_metadata;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.statuses);
        dest.writeParcelable(this.search_metadata, flags);
    }

    protected ModelSearchTweets(Parcel in) {
        this.statuses = in.createTypedArrayList(ModelSearchTweetsStatus.CREATOR);
        this.search_metadata = in.readParcelable(ModelSearchTweetsMetaData.class.getClassLoader());
    }

    public static final Parcelable.Creator<ModelSearchTweets> CREATOR = new Parcelable.Creator<ModelSearchTweets>() {
        @Override
        public ModelSearchTweets createFromParcel(Parcel source) {
            return new ModelSearchTweets(source);
        }

        @Override
        public ModelSearchTweets[] newArray(int size) {
            return new ModelSearchTweets[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweets{" +
                "statuses=" + statuses +
                ", search_metadata=" + search_metadata +
                '}';
    }
}
