package com.pm.twitterhashtagsearch.controllers.network.retrofit;

/**
 * Created by faisalwahab on 1/31/16.
 */
public interface IRetrofitCallBack {
    void onCallBack(Object ref, Object data, Object action, int status);
}