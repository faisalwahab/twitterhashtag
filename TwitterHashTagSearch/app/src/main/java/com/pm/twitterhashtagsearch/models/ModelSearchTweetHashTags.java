package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweetHashTags implements Parcelable {
    private String text;

    public ModelSearchTweetHashTags() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
    }

    protected ModelSearchTweetHashTags(Parcel in) {
        this.text = in.readString();
    }

    public static final Creator<ModelSearchTweetHashTags> CREATOR = new Creator<ModelSearchTweetHashTags>() {
        @Override
        public ModelSearchTweetHashTags createFromParcel(Parcel source) {
            return new ModelSearchTweetHashTags(source);
        }

        @Override
        public ModelSearchTweetHashTags[] newArray(int size) {
            return new ModelSearchTweetHashTags[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweetHashTags{" +
                "text='" + text + '\'' +
                '}';
    }
}
