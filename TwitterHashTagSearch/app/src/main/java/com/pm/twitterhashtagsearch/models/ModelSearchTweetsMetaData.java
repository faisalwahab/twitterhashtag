package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweetsMetaData implements Parcelable {
    private float completed_in;
    private long max_id;
    private String max_id_str;
    private String query;
    private String refresh_url;
    private int count;

    public ModelSearchTweetsMetaData() {
    }

    public float getCompleted_in() {
        return completed_in;
    }

    public void setCompleted_in(float completed_in) {
        this.completed_in = completed_in;
    }

    public long getMax_id() {
        return max_id;
    }

    public void setMax_id(long max_id) {
        this.max_id = max_id;
    }

    public String getMax_id_str() {
        return max_id_str;
    }

    public void setMax_id_str(String max_id_str) {
        this.max_id_str = max_id_str;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRefresh_url() {
        return refresh_url;
    }

    public void setRefresh_url(String refresh_url) {
        this.refresh_url = refresh_url;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.completed_in);
        dest.writeLong(this.max_id);
        dest.writeString(this.max_id_str);
        dest.writeString(this.query);
        dest.writeString(this.refresh_url);
        dest.writeInt(this.count);
    }

    protected ModelSearchTweetsMetaData(Parcel in) {
        this.completed_in = in.readFloat();
        this.max_id = in.readLong();
        this.max_id_str = in.readString();
        this.query = in.readString();
        this.refresh_url = in.readString();
        this.count = in.readInt();
    }

    public static final Creator<ModelSearchTweetsMetaData> CREATOR = new Creator<ModelSearchTweetsMetaData>() {
        @Override
        public ModelSearchTweetsMetaData createFromParcel(Parcel source) {
            return new ModelSearchTweetsMetaData(source);
        }

        @Override
        public ModelSearchTweetsMetaData[] newArray(int size) {
            return new ModelSearchTweetsMetaData[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweetsMetaData{" +
                "completed_in=" + completed_in +
                ", max_id=" + max_id +
                ", max_id_str='" + max_id_str + '\'' +
                ", query='" + query + '\'' +
                ", refresh_url='" + refresh_url + '\'' +
                ", count=" + count +
                '}';
    }
}
