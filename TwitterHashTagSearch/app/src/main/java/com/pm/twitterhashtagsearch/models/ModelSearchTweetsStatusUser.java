package com.pm.twitterhashtagsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faisalwahab on 1/7/17.
 */

public class ModelSearchTweetsStatusUser implements Parcelable {
    private long id;
    private String id_str;
    private String name;
    private String screen_name;
    private String description;
    private int followers_count;
    private int friends_count;
    private int listed_count;
    private String created_at;
    private int favourites_count;
    private int statuses_count;
    private String lang;
    private String profile_background_color;
    private String profile_image_url;
    private String profile_image_url_https;
    private String profile_banner_url;
    private String profile_link_color;
    private String profile_sidebar_border_color;
    private String profile_sidebar_fill_color;
    private String profile_text_color;
    private String translator_type;


    public ModelSearchTweetsStatusUser() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId_str() {
        return id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(int friends_count) {
        this.friends_count = friends_count;
    }

    public int getListed_count() {
        return listed_count;
    }

    public void setListed_count(int listed_count) {
        this.listed_count = listed_count;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getFavourites_count() {
        return favourites_count;
    }

    public void setFavourites_count(int favourites_count) {
        this.favourites_count = favourites_count;
    }

    public int getStatuses_count() {
        return statuses_count;
    }

    public void setStatuses_count(int statuses_count) {
        this.statuses_count = statuses_count;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getProfile_background_color() {
        return profile_background_color;
    }

    public void setProfile_background_color(String profile_background_color) {
        this.profile_background_color = profile_background_color;
    }

    public String getProfile_image_url() {
        return profile_image_url;
    }

    public void setProfile_image_url(String profile_image_url) {
        this.profile_image_url = profile_image_url;
    }

    public String getProfile_image_url_https() {
        return profile_image_url_https;
    }

    public void setProfile_image_url_https(String profile_image_url_https) {
        this.profile_image_url_https = profile_image_url_https;
    }

    public String getProfile_banner_url() {
        return profile_banner_url;
    }

    public void setProfile_banner_url(String profile_banner_url) {
        this.profile_banner_url = profile_banner_url;
    }

    public String getProfile_link_color() {
        return profile_link_color;
    }

    public void setProfile_link_color(String profile_link_color) {
        this.profile_link_color = profile_link_color;
    }

    public String getProfile_sidebar_border_color() {
        return profile_sidebar_border_color;
    }

    public void setProfile_sidebar_border_color(String profile_sidebar_border_color) {
        this.profile_sidebar_border_color = profile_sidebar_border_color;
    }

    public String getProfile_sidebar_fill_color() {
        return profile_sidebar_fill_color;
    }

    public void setProfile_sidebar_fill_color(String profile_sidebar_fill_color) {
        this.profile_sidebar_fill_color = profile_sidebar_fill_color;
    }

    public String getProfile_text_color() {
        return profile_text_color;
    }

    public void setProfile_text_color(String profile_text_color) {
        this.profile_text_color = profile_text_color;
    }

    public String getTranslator_type() {
        return translator_type;
    }

    public void setTranslator_type(String translator_type) {
        this.translator_type = translator_type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.id_str);
        dest.writeString(this.name);
        dest.writeString(this.screen_name);
        dest.writeString(this.description);
        dest.writeInt(this.followers_count);
        dest.writeInt(this.friends_count);
        dest.writeInt(this.listed_count);
        dest.writeString(this.created_at);
        dest.writeInt(this.favourites_count);
        dest.writeInt(this.statuses_count);
        dest.writeString(this.lang);
        dest.writeString(this.profile_background_color);
        dest.writeString(this.profile_image_url);
        dest.writeString(this.profile_image_url_https);
        dest.writeString(this.profile_banner_url);
        dest.writeString(this.profile_link_color);
        dest.writeString(this.profile_sidebar_border_color);
        dest.writeString(this.profile_sidebar_fill_color);
        dest.writeString(this.profile_text_color);
        dest.writeString(this.translator_type);
    }

    protected ModelSearchTweetsStatusUser(Parcel in) {
        this.id = in.readLong();
        this.id_str = in.readString();
        this.name = in.readString();
        this.screen_name = in.readString();
        this.description = in.readString();
        this.followers_count = in.readInt();
        this.friends_count = in.readInt();
        this.listed_count = in.readInt();
        this.created_at = in.readString();
        this.favourites_count = in.readInt();
        this.statuses_count = in.readInt();
        this.lang = in.readString();
        this.profile_background_color = in.readString();
        this.profile_image_url = in.readString();
        this.profile_image_url_https = in.readString();
        this.profile_banner_url = in.readString();
        this.profile_link_color = in.readString();
        this.profile_sidebar_border_color = in.readString();
        this.profile_sidebar_fill_color = in.readString();
        this.profile_text_color = in.readString();
        this.translator_type = in.readString();
    }

    public static final Creator<ModelSearchTweetsStatusUser> CREATOR = new Creator<ModelSearchTweetsStatusUser>() {
        @Override
        public ModelSearchTweetsStatusUser createFromParcel(Parcel source) {
            return new ModelSearchTweetsStatusUser(source);
        }

        @Override
        public ModelSearchTweetsStatusUser[] newArray(int size) {
            return new ModelSearchTweetsStatusUser[size];
        }
    };

    @Override
    public String toString() {
        return "ModelSearchTweetsStatusUser{" +
                "id=" + id +
                ", id_str='" + id_str + '\'' +
                ", name='" + name + '\'' +
                ", screen_name='" + screen_name + '\'' +
                ", description='" + description + '\'' +
                ", followers_count=" + followers_count +
                ", friends_count=" + friends_count +
                ", listed_count=" + listed_count +
                ", created_at='" + created_at + '\'' +
                ", favourites_count=" + favourites_count +
                ", statuses_count=" + statuses_count +
                ", lang='" + lang + '\'' +
                ", profile_background_color='" + profile_background_color + '\'' +
                ", profile_image_url='" + profile_image_url + '\'' +
                ", profile_image_url_https='" + profile_image_url_https + '\'' +
                ", profile_banner_url='" + profile_banner_url + '\'' +
                ", profile_link_color='" + profile_link_color + '\'' +
                ", profile_sidebar_border_color='" + profile_sidebar_border_color + '\'' +
                ", profile_sidebar_fill_color='" + profile_sidebar_fill_color + '\'' +
                ", profile_text_color='" + profile_text_color + '\'' +
                ", translator_type='" + translator_type + '\'' +
                '}';
    }
}
