package com.pm.twitterhashtagsearch.controllers.network.datamanagers.apiservices;


import com.pm.twitterhashtagsearch.models.ModelSearchTweets;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by faisalwahab on 1/31/16.
 */
public interface GeneralServices {

    @Headers("Authorization: Bearer AAAAAAAAAAAAAAAAAAAAALGHwgAAAAAA2QmNYZGtQOnOeDufD5%2F%2BU9HZNvE%3DAOPl26qvFHxYI3CwuwuymGJn05rExOcwYEejAzy7r3l6CTg6Rk")
    @GET("search/tweets.json")
    Call<ModelSearchTweets> fetchTweets(
            @Query("q") String hashTag,
            @Query("since_id") String sinceId,
            @Query("count") String count
    );

}
