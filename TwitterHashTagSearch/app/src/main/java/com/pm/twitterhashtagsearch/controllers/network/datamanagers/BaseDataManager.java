package com.pm.twitterhashtagsearch.controllers.network.datamanagers;


import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

import com.pm.twitterhashtagsearch.controllers.network.datamanagers.apiservices.GeneralServices;
import com.pm.twitterhashtagsearch.controllers.network.retrofit.ClientFactory;
import com.pm.twitterhashtagsearch.controllers.network.retrofit.ErrorUtils;
import com.pm.twitterhashtagsearch.controllers.network.retrofit.IRetrofitCallBack;
import com.pm.twitterhashtagsearch.controllers.network.utils.ApiConstants;
import com.pm.twitterhashtagsearch.controllers.utilities.ApplicationUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by faisalwahab on 1/31/16.
 */
public class BaseDataManager {

    public GeneralServices getGeneralsService(ClientFactory.ClientType clientType) {
        return ClientFactory.getInstance().getClient(clientType).getRetrofitService(GeneralServices.class);
    }


    /**
     * @param call              Retrofit service object
     * @param iRetrofitCallBack Call back object to send api response back
     * @param activity          Activity object to show  UI stuff if you set null then UI stuff will not show
     */
    public <T> void retrofit2Api(Call<T> call, final IRetrofitCallBack iRetrofitCallBack, final boolean isProgressDialogShow, final FragmentActivity activity) {
        final ProgressDialog pd = ApplicationUtils.getProgressDialog(activity);
        if (isProgressDialogShow)
            showProgressInUiThread(pd, activity);

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (isProgressDialogShow)
                    dismissProgressInUiThread(pd, activity);

                if (response.isSuccessful()) {

                    if (iRetrofitCallBack != null) {
                        iRetrofitCallBack.onCallBack(BaseDataManager.this, response.body(), null, ApiConstants.SUCCESS);
                    }

                } else {

                    // retrofit error response
                    showToastInUiThread(response.errorBody().toString(), activity);

                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                if (isProgressDialogShow)
                    dismissProgressInUiThread(pd, activity);

                // Log error here since request failed
                t.printStackTrace();

                if (!ApplicationUtils.isNetworkAvailable()) {

                    // reset api logic
                    getReset(call, this, pd, activity, isProgressDialogShow);

                } else {
                    // Android Dialog
                    // Notice
                    // Application facing some Technical issue please reset the application
                    if (t != null && t.getCause() != null)
                        showToastInUiThread("AndroidDialog: " + t.getCause().toString(), activity);
                }
            }
        });
    }

    /**
     * @param call     Retrofit service object
     * @param callback Retrofit callback object
     * @param pd       Progress Dialog object
     * @param activity activity object to show UI stuff
     */
    public <T> void getReset(final Call<T> call, final Callback<T> callback, final ProgressDialog pd, final FragmentActivity activity, final boolean isProgressDialogShow) {

        // reset api logic
        ErrorUtils.parseError(ApiConstants.FAILURE, ApiConstants.ERROR_NETWORK_NOT_AVAILABLE, new IRetrofitCallBack() {
            @Override
            public void onCallBack(Object ref, Object data, Object action, int status) {
                if (status == ApiConstants.RESET) {
                    if (isProgressDialogShow)
                        showProgressInUiThread(pd, activity);

                    call.clone().enqueue(callback);

                }
            }
        }, activity);

    }

    /**
     * @param message  Text message to show in the toast.
     * @param activity activity object to show toast in UI thread, because after reset retrofit api UI stuff will not works because of the background thread
     */
    void showToastInUiThread(final String message, Activity activity) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ApplicationUtils.showToast(message);
                }
            });
        }
    }


    /**
     * @param pd       Progress Dialog
     * @param activity activity object to show toast in UI thread, because after reset retrofit api UI stuff will not works because of the background thread
     */
    void showProgressInUiThread(final ProgressDialog pd, Activity activity) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ApplicationUtils.showProgressDialog(pd);
                }
            });
        }
    }

    /**
     * @param pd       Progress Dialog
     * @param activity activity object to show toast in UI thread, because after reset retrofit api UI stuff will not works because of the background thread
     */
    void dismissProgressInUiThread(final ProgressDialog pd, Activity activity) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ApplicationUtils.dismissProgressDialog(pd);
                }
            });
        }
    }
}
